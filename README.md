# Simple Terms And Menus

For websites with a simple menu or taxonomy setup, the default menu and term 
forms can be a little bulky.

This modules does some cleanup on the menu and taxonomy forms, and opens add and
edit links inside a modal.

## Usage

Enable the module as usual.

Give 'Use advanced menu options' to all roles that need to be able to still use 
the original forms.
