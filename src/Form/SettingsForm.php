<?php

namespace Drupal\simple_terms_and_menus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings form for the Simple terms and menu module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'simple_terms_and_menus_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['simple_terms_and_menus.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['disable_menu_modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable menu modal'),
      '#description' => $this->t('Disable displaying edit menu in a modal'),
      '#default_value' => $this->config('simple_terms_and_menus.settings')->get('disable_menu_modal'),
    ];

    $form['disable_term_modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable term modal'),
      '#description' => $this->t('Disable displaying edit term in a modal'),
      '#default_value' => $this->config('simple_terms_and_menus.settings')->get('disable_term_modal'),
    ];

    $form['disable_media_modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable media modal'),
      '#description' => $this->t('Disable displaying edit media in a modal'),
      '#default_value' => $this->config('simple_terms_and_menus.settings')->get('disable_media_modal'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('simple_terms_and_menus.settings')
      ->set('disable_menu_modal', $form_state->getValue('disable_menu_modal'))
      ->set('disable_term_modal', $form_state->getValue('disable_term_modal'))
      ->set('disable_media_modal', $form_state->getValue('disable_media_modal'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
